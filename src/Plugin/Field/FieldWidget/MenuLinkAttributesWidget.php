<?php

namespace Drupal\menu_link_field_attributes\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\menu_link\Plugin\Field\FieldWidget\MenuLinkWidget;
use Drupal\menu_link_field_attributes\Render\Element\MenuAttributesDetails;

/**
 * Provides a widget for the menu_link field type.
 *
 * @FieldWidget(
 *   id = "menu_link_attributes",
 *   label = @Translation("Menu link with attributes"),
 *   field_types = {
 *     "menu_link"
 *   }
 * )
 */
class MenuLinkAttributesWidget extends MenuLinkWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    unset($element['#pre_render']);

    $element['#pre_render'][] = [MenuAttributesDetails::class, 'preRender'];

    $item = $items[$delta];

    $options = $item->get('options')->getValue();
    $attributes = $options['attributes'] ?? [];

    $element['options']['attributes'] = [
      '#type' => 'details',
      '#title' => $this->t('Attributes'),
      '#tree' => TRUE,
      '#open' => count($attributes),
    ];

    $class_default_value = $attributes['class'] ?? NULL;
    if (is_array($class_default_value)) {
      $class_default_value = implode(' ', $attributes['class']);
    }

    $enabled_attributes = [
      'name' => [
        'title' => $this->t('Name'),
      ],
      'rel' => [
        'title' => $this->t('Relationship'),
      ],
      'class' => [
        'title' => $this->t('Class'),
      ],
      'accesskey' => [
        'title' => $this->t('AccessKey'),
      ],
    ];

    foreach ($enabled_attributes as $attribute_key => $attribute_settings) {
      $default_value = $attributes[$attribute_key] ?? NULL;
      if ($attribute_key == 'class' && is_array($default_value)) {
        $default_value = implode(' ', $attributes[$attribute_key]);
      }

      $element['options']['attributes'][$attribute_key] = [
        '#title' => $attribute_settings['title'],
        '#type' => (isset($attribute_settings['type'])) ? $attribute_settings['type'] : 'textfield',
        '#default_value' => $default_value,
        '#description' => (isset($attribute_settings['description'])) ? $attribute_settings['description'] : '',
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Convert a class string to an array so that it can be merged reliable.
    foreach ($values as $delta => $value) {
      if (isset($value['options']['attributes']['class']) && is_string($value['options']['attributes']['class'])) {
        $values[$delta]['options']['attributes']['class'] = explode(' ', $value['options']['attributes']['class']);
      }
    }

    return array_map(function (array $value) {
      if (isset($value['options']['attributes'])) {
        $value['options']['attributes'] = array_filter($value['options']['attributes'], function ($attribute) {
          return $attribute !== "";
        });
      }
      return $value;
    }, $values);
  }

}
