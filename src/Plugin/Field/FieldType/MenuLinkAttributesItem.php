<?php

namespace Drupal\menu_link_field_attributes\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\menu_link\Plugin\Field\FieldType\MenuLinkItem;

/**
 * Defines a menu link field type which stores the link, parent and menu.
 *
 * @FieldType(
 *   id = "menu_link",
 *   label = @Translation("Menu link"),
 *   description = @Translation("Stores a title, menu and parent to insert a
 *   link to the current entity."), default_widget = "menu_link_default",
 *   list_class = "\Drupal\menu_link\Plugin\Field\MenuLinkItemList",
 *   default_formatter = "menu_link", column_groups = {
 *     "title" = {
 *       "label" = @Translation("Title"),
 *       "translatable" = TRUE
 *     },
 *     "description" = {
 *       "label" = @Translation("Description"),
 *       "translatable" = TRUE
 *     },
 *     "menu_name" = {
 *       "label" = @Translation("Menu name"),
 *       "translatable" = TRUE
 *     },
 *     "parent" = {
 *       "label" = @Translation("Parent"),
 *       "translatable" = TRUE
 *     },
 *   },
 * )
 */
class MenuLinkAttributesItem extends MenuLinkItem {

  /**
   *
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $definitions = parent::propertyDefinitions($field_definition);

    $definitions['options'] = DataDefinition::create('any')->setLabel(t('Menu link options'));

    return $definitions;
  }

  /**
   *
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['options'] = [
      'description' => 'A serialized array of URL options, such as a query string or HTML attributes.',
      'type' => 'blob',
      'size' => 'big',
      'not null' => FALSE,
      'serialize' => TRUE,
    ];

    return $schema;
  }

  /**
   * {@inheritDoc }
   */
  protected function getMenuPluginDefinition($langcode) {
    $menu_definition = parent::getMenuPluginDefinition($langcode);

    $menu_definition['options'] = isset($this->values['options']) ? $this->values['options'] : [];

    return $menu_definition;
  }

}
