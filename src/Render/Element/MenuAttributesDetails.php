<?php

namespace Drupal\menu_link_field_attributes\Render\Element;

use Drupal\menu_link\Render\Element\MenuDetails;

/**
 * {@inheritDoc}
 */
class MenuAttributesDetails extends MenuDetails {

  /**
   * {@inheritDoc}
   */
  public static function preRender($element) {
    $element = parent::preRender($element);

    $element['menu']['options'] = $element['options'];
    $element["menu"]["options"]["#weight"] += $element["menu"]["description"]["#weight"];
    $element['menu']['options']['attributes']['#states'] = [
      'invisible' => [
        'input[name="' . $element['menu']['enabled']['#name'] . '"]' => ['checked' => FALSE],
      ],
    ];
    unset($element['options']);

    return $element;
  }

}
